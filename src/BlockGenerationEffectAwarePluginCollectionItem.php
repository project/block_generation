<?php

namespace Drupal\block_generation;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\plugin\Plugin\Field\FieldType\PluginCollectionItem;

/**
 * Provides a plugin item for block effect plugins.
 */
class BlockGenerationEffectAwarePluginCollectionItem extends PluginCollectionItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['plugin_instance'] = MapDataDefinition::create('block_generation_effect_aware_plugin_instance')
      ->setLabel(t('Plugin instance'))
      ->setComputed(TRUE);

    return $properties;
  }

}
