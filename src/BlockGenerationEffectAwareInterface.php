<?php

namespace Drupal\block_generation;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an object that can be aware of an entity which contains this plugin.
 */
interface BlockGenerationEffectAwareInterface {

  /**
   * Sets the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return $this
   */
  public function setEntity(EntityInterface $entity);

  /**
   * Gets the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity();

}
