<?php

namespace Drupal\block_generation;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\plugin\PluginOperationsProviderProviderInterface;

/**
 * Defines a plugin manager.
 */
interface BlockGenerationEffectManagerInterface extends PluginOperationsProviderProviderInterface, PluginManagerInterface {

  /**
   * Creates a plugin.
   *
   * @param string $plugin_id
   *   The id of the plugin being instantiated.
   * @param mixed[] $configuration
   *   An array of configuration relevant to the plugin instance.
   *
   * @return \Drupal\block_generation\BlockGenerationEffectPluginInterface
   */
  public function createInstance($plugin_id, array $configuration = []);

}
