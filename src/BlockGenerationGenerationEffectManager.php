<?php

namespace Drupal\block_generation;

use Drupal\block_generation\Annotation\BlockGenerationEffect;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\plugin\Plugin\PluginOperationsProviderPluginManagerTrait;

/**
 * Manages discovery and instantiation of plugins.
 *
 * @see \Drupal\block_generation\BlockGenerationEffectPluginInterface
 */
class BlockGenerationGenerationEffectManager extends DefaultPluginManager implements BlockGenerationEffectManagerInterface {

  use PluginOperationsProviderPluginManagerTrait;

  /**
   * Constructs a new instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class_resolver.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ClassResolverInterface $class_resolver) {
    parent::__construct('Plugin/BlockGenerationEffect', $namespaces, $module_handler, BlockGenerationEffectPluginInterface::class, BlockGenerationEffect::class);
    $this->alterInfo('block_generation_effect');
    $this->setCacheBackend($cache_backend, 'block_generation_effect', ['block_generation_effect']);
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    return $definitions;
  }

}
