<?php

namespace Drupal\block_generation;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * A plugin.
 */
interface BlockGenerationEffectPluginInterface extends PluginInspectionInterface, BlockGenerationEffectAwareInterface, ConfigurableInterface, DependentPluginInterface, PluginFormInterface {

  /**
   * Returns the description of plugin.
   *
   * @param string|\Drupal\Core\StringTranslation\TranslationWrapper
   */
  public function getDescription();

  /**
   * @param $wrapper
   * @param array $variables
   *
   * @return mixed
   */
  public function applyEffect($wrapper, array &$variables);

}
