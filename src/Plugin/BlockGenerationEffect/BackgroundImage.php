<?php

namespace Drupal\block_generation\Plugin\BlockGenerationEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\file\Entity\File;

/**
 * @BlockGenerationEffect(
 *   id = "background_image",
 *   label = @Translation("Background image"),
 *   description = @Translation("Background image block effect.")
 * )
 */
class BackgroundImage extends BlockGenerationEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = [
      'fid' => NULL,
      'position' => NULL,
      'repeat' => NULL,
      'size' => NULL,
    ];

    return NestedArray::mergeDeep($default, parent::defaultConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    if (is_array($this->configuration['fid'])) {
      $this->configuration['fid'] = reset($this->configuration['fid']);
    }

    $form['fid'] = [
      '#title' => $this->t('Image'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://i/effect',
      '#default_value' => !empty($this->configuration['fid']) ? [$this->configuration['fid']] : $default['fid'],
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg svg'],
        'file_validate_size' => [8*1024*1024],
      ],
      '#required' => TRUE,
    ];

    $form['position'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Position'),
      '#default_value' => !empty($this->configuration['position']) ? $this->configuration['position'] : $default['position'],
      '#description' => $this->t('Position of the image for the background.'),
    );

    $form['repeat'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Repeat'),
      '#default_value' => !empty($this->configuration['repeat']) ? $this->configuration['repeat'] : $default['repeat'],
      '#description' => $this->t('Repeat of the image for the background.'),
    );

    $form['size'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Size'),
      '#default_value' => !empty($this->configuration['size']) ? $this->configuration['size'] : $default['size'],
      '#description' => $this->t('Size of the image for the background.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    if ($fid = is_array($values['fid']) ? reset($values['fid']) : $values['fid']) {
      $this->configuration['fid'] = $fid;
      $this->configuration['position'] = $values['position'];
      $this->configuration['repeat'] = $values['repeat'];
      $this->configuration['size'] = $values['size'];

      parent::submitConfigurationForm($form, $form_state);

      // Determine whether it was the upload or the remove button that was clicked,
      // and set $element to the managed_file element that contains that button.
      $parents = $form_state->getTriggeringElement()['#array_parents'];
      $button_key = array_pop($parents);

      $file = File::load($fid);
      if ($button_key == 'submit') {
        // Update file usage for background image.
        \Drupal::service('file.usage')->add($file, 'file', $this->entity->getEntityTypeId() . ':' . $this->getPluginId(), $this->entity->id());
      }
      elseif ($button_key == 'remove_button') {
        // If file is temporary then delete it, otherwise delete it from file usage.
        if ($file->isTemporary()) {
          $file->delete();
        }
        else {
          \Drupal::service('file.usage')->delete($file, 'file', $this->entity->getEntityTypeId() . ':' . $this->getPluginId(), $this->entity->id());
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect($wrapper, array &$variables) {
    if (!empty($variables['settings'][$wrapper]['attributes']->offsetGet('style')) && $styles = explode(';', $variables['settings'][$wrapper]['attributes']->offsetGet('style'))) {
      // Prepare styles to be updated.
      foreach ($styles as $delta => &$style) {
        $style = trim($style);
        if ($style === '') {
          unset($styles[$delta]);
        }
      }
    }
    // Apply effect.
    if ($file = \Drupal::service('entity_type.manager')->getStorage('file')->load($this->configuration['fid'])) {
      $file_url = \Drupal::service('file_url_generator')->generateString($file->getFileUri());

      // Background image.
      if ($file_url) {
        $styles[] = 'background-image: url(\''.$file_url.'\')';
        $styles[] = 'background-image: -webkit-url(\''.$file_url.'\')';
      }

      // background-size;
      if (isset($this->configuration['size']) && $this->configuration['size']) {
        $styles[] = 'background-size: ' . $this->configuration['size'];
      }

      // background-position;
      if (isset($this->configuration['position']) && $this->configuration['position']) {
        $styles[] = 'background-position: '.$this->configuration['position'];
      }

      // background-repeat;
      if (isset($this->configuration['repeat']) && $this->configuration['repeat']) {
        $styles[] = 'background-repeat: ' . $this->configuration['repeat'];
      }

      // Compose styles.
      $variables['settings'][$wrapper]['attributes']->setAttribute('style', implode('; ', $styles) . ';');
    }
  }

}
