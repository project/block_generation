<?php

namespace Drupal\block_generation\Plugin\BlockGenerationEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * @BlockGenerationEffect(
 *   id = "attached_library",
 *   label = @Translation("Attached Library"),
 *   description = @Translation("Attaches any pre-defined library.")
 * )
 */
class AttachedLibrary extends BlockGenerationEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = [
      'attached_library' => NULL,
    ];

    return NestedArray::mergeDeep($default, parent::defaultConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['attached_library'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Library'),
      '#default_value' => !empty($this->configuration['attached_library']) ? $this->configuration['attached_library'] : $default['attached_library'],
      '#description' => $this->t('Library to be attached.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    $this->configuration['attached_library'] = $values['attached_library'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect($wrapper, array &$variables) {
    if (isset($this->configuration['attached_library']) && !empty($this->configuration['attached_library'])) {
      // Apply effect.
      $variables['#attached']['library'][] = $this->configuration['attached_library'];
    }
  }

}
