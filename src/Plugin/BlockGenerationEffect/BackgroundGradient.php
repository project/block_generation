<?php

namespace Drupal\block_generation\Plugin\BlockGenerationEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * @BlockGenerationEffect(
 *   id = "background_gradient",
 *   label = @Translation("Background gradient"),
 *   description = @Translation("Background gradient block effect.")
 * )
 */
class BackgroundGradient extends BlockGenerationEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = [
      'type' => NULL,
      'direction' => NULL,
      'color' => NULL,
    ];

    return NestedArray::mergeDeep($default, parent::defaultConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $gradients = [
      'linear' => 'Linear',
      'radial' => 'Radial',
      'conic' => 'Conic',
    ];

    $form['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => $gradients,
      '#default_value' => !empty($this->configuration['type']) ? $this->configuration['type'] : $default['type'],
      '#description' => $this->t('The type of the gradient.'),
    );

    $form['direction'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Direction'),
      '#default_value' => !empty($this->configuration['direction']) ? $this->configuration['direction'] : $default['direction'],
      '#description' => $this->t('Directions for gradient of the background.'),
    );

    $form['color'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Colors for gradient'),
      '#default_value' => !empty($this->configuration['color']) ? $this->configuration['color'] : $default['color'],
      '#description' => $this->t('Colors for gradient of the background.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    $this->configuration['type'] = $values['type'];
    $this->configuration['direction'] = $values['direction'];
    $this->configuration['color'] = $values['color'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect($wrapper, array &$variables) {
    if (!empty($variables['settings'][$wrapper]['attributes']->offsetGet('style')) && $styles = explode(';', $variables['settings'][$wrapper]['attributes']->offsetGet('style'))) {
      // Prepare styles to be updated.
      foreach ($styles as $delta => &$style) {
        $style = trim($style);
        if ($style === '') {
          unset($styles[$delta]);
        }
      }
    }
    // Apply effect.
    list($color1, $color2) = explode(',', $this->configuration['color']);
    $color1 = trim($color1);
    $color2 = trim($color2);
    switch ($this->configuration['type']) {
      case 'linear':
        // background-image: -webkit-gradient(linear, left top, right top, from(red), to(#f06d06));
        $styles[] = 'background-image: -webkit-gradient(linear, '.$this->configuration['direction'].', from('.$color1.'), to('.$color2.'))';
        // background-image: -webkit-linear-gradient(left, red, #f06d06);
        $styles[] = 'background-image: -webkit-linear-gradient('.$this->configuration['direction'].', '.$this->configuration['color'].')';
        // background-image: -moz-linear-gradient(left, red, #f06d06);
        $styles[] = 'background-image: -moz-linear-gradient('.$this->configuration['direction'].', '.$this->configuration['color'].')';
        // background-image: -o-linear-gradient(left, red, #f06d06);
        $styles[] = 'background-image: -o-linear-gradient('.$this->configuration['direction'].', '.$this->configuration['color'].')';
        // background-image: linear-gradient(left, red, #f06d06);
        $styles[] = 'background-image: linear-gradient('.$this->configuration['direction'].', '.$this->configuration['color'].')';
        break;

      case 'radial':
        break;

      case 'conic':
        break;
    }

    // Compose styles.
    $variables['settings'][$wrapper]['attributes']->setAttribute('style', implode('; ', $styles) . ';');
  }

}
