<?php

namespace Drupal\block_generation\Plugin\BlockGenerationEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * @BlockGenerationEffect(
 *   id = "custom_class",
 *   label = @Translation("Class"),
 *   description = @Translation("Custom classes for block.")
 * )
 */
class CustomClass extends BlockGenerationEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = [
      'class' => NULL,
    ];

    return NestedArray::mergeDeep($default, parent::defaultConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#default_value' => !empty($this->configuration['class']) ? $this->configuration['class'] : $default['class'],
      '#description' => $this->t('Classes for the block.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    $this->configuration['class'] = $values['class'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect($wrapper, array &$variables) {
    if (isset($this->configuration['class']) && !empty($this->configuration['class'])) {
      // Apply effect.
      $classes_to_apply = explode(' ', $this->configuration['class']);
      foreach ($classes_to_apply as $delta => &$class_to_apply) {
        $class_to_apply = trim($class_to_apply);
        $variables['settings'][$wrapper]['attributes']->addClass($class_to_apply);
      }
    }
  }

}
