<?php

namespace Drupal\block_generation\Plugin\BlockGenerationEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * @BlockGenerationEffect(
 *   id = "leading_text",
 *   label = @Translation("Leading Text"),
 *   description = @Translation("Custom leading text.")
 * )
 */
class LeadingText extends BlockGenerationEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = [
      'leading_text' => NULL,
    ];

    return NestedArray::mergeDeep($default, parent::defaultConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['leading_text'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Leading Text'),
      '#default_value' => !empty($this->configuration['leading_text']) ? $this->configuration['leading_text'] : $default['leading_text'],
      '#description' => $this->t('Custom leading text for the wrapper.'),
      '#rows' => 3,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    $this->configuration['leading_text'] = $values['leading_text'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect($wrapper, array &$variables) {
    if (isset($this->configuration['leading_text']) && !empty($this->configuration['leading_text'])) {
      // Apply effect.
      if (isset($variables['items']) && is_array($variables['items'])) {
        foreach ($variables['items'] as &$item) {
          $item['content']['#prefix'] = $this->configuration['leading_text'];
        }
      }
    }
  }

}
