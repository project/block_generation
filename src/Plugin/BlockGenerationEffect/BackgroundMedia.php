<?php

namespace Drupal\block_generation\Plugin\BlockGenerationEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\file\Entity\File;

/**
 * @BlockGenerationEffect(
 *   id = "background_media",
 *   label = @Translation("Background media"),
 *   description = @Translation("Background media block effect.")
 * )
 */
class BackgroundMedia extends BlockGenerationEffectBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default = [
      'id' => NULL,
      'image_style' => NULL,
      'position' => NULL,
      'repeat' => NULL,
      'size' => NULL,
    ];

    return NestedArray::mergeDeep($default, parent::defaultConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $default = $this->defaultConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);

    $media_types = [];
    foreach ($this->entityTypeManager->getStorage('media_type')->loadMultiple() as $id => $media_type) {
      $media_types[] = $id;
    }

    $form['id'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => $media_types,
      '#cardinality' => 1,
      '#title' => t('Media'),
      '#default_value' => !empty($this->configuration['id']) ? $this->configuration['id'] : $default['id'],
      '#description' => t('Upload or select your media object.'),
      '#required' => TRUE,
    ];

    $style_options = [];
    foreach ($this->entityTypeManager->getStorage('image_style')->loadMultiple() as $id => $image_style) {
      $style_options[$id] = $image_style->label();
    }

    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Style'),
      '#description' => $this->t('Select image style to be applied.'),
      '#default_value' => !empty($this->configuration['image_style']) ? [$this->configuration['image_style']] : $default['image_style'],
      '#options' => $style_options,
      '#required' => TRUE,
    ];

    $form['position'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Position'),
      '#default_value' => !empty($this->configuration['position']) ? $this->configuration['position'] : $default['position'],
      '#description' => $this->t('Position of the image for the background.'),
    );

    $form['repeat'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Repeat'),
      '#default_value' => !empty($this->configuration['repeat']) ? $this->configuration['repeat'] : $default['repeat'],
      '#description' => $this->t('Repeat of the image for the background.'),
    );

    $form['size'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Size'),
      '#default_value' => !empty($this->configuration['size']) ? $this->configuration['size'] : $default['size'],
      '#description' => $this->t('Size of the image for the background.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $form['#parents']);

    if ($id = is_array($values['id']) ? reset($values['id']) : $values['id']) {
      $this->configuration['id'] = $id;
      $this->configuration['image_style'] = $values['image_style'];
      $this->configuration['position'] = $values['position'];
      $this->configuration['repeat'] = $values['repeat'];
      $this->configuration['size'] = $values['size'];

      parent::submitConfigurationForm($form, $form_state);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect($wrapper, array &$variables) {
    if (!empty($variables['settings'][$wrapper]['attributes']->offsetGet('style')) && $styles = explode(';', $variables['settings'][$wrapper]['attributes']->offsetGet('style'))) {
      // Prepare styles to be updated.
      foreach ($styles as $delta => &$style) {
        $style = trim($style);
        if ($style === '') {
          unset($styles[$delta]);
        }
      }
    }
    // Apply effect.
    if ($media = $this->entityTypeManager->getStorage('media')->load($this->configuration['id'])) {
      $fid = $media->getSource()->getSourceFieldValue($media);
      $file = $this->entityTypeManager->getStorage('file')->load($fid);
      $file_uri = $file->getFileUri();

      // Apply selected image style.
      if ($image_style = $this->entityTypeManager->getStorage('image_style')->load($this->configuration['image_style'])) {
        $file_url = $image_style->buildUrl($file_uri);
      }
      else {
        $file_url = \Drupal::service('file_url_generator')->generateString($file_uri);
      }

      // Background image.
      if ($file_url) {
        $styles[] = 'background-image: url(\''.$file_url.'\')';
        $styles[] = 'background-image: -webkit-url(\''.$file_url.'\')';
      }

      // background-size;
      if (isset($this->configuration['size']) && $this->configuration['size']) {
        $styles[] = 'background-size: ' . $this->configuration['size'];
      }

      // background-position;
      if (isset($this->configuration['position']) && $this->configuration['position']) {
        $styles[] = 'background-position: '.$this->configuration['position'];
      }

      // background-repeat;
      if (isset($this->configuration['repeat']) && $this->configuration['repeat']) {
        $styles[] = 'background-repeat: ' . $this->configuration['repeat'];
      }

      // Compose styles.
      $variables['settings'][$wrapper]['attributes']->setAttribute('style', implode('; ', $styles) . ';');
    }
  }

}
