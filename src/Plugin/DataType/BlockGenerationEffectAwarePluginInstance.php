<?php

namespace Drupal\block_generation\Plugin\DataType;

use Drupal\block_generation\BlockGenerationEffectAwareInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\plugin\Plugin\DataType\PluginInstance;

/**
 * Provides a variable plugin instance data type.
 *
 * @DataType(
 *   id = "block_generation_effect_aware_plugin_instance",
 *   label = @Translation("Block Generation Effect plugin instance")
 * )
 */
class BlockGenerationEffectAwarePluginInstance extends PluginInstance {

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    if ($value instanceof BlockGenerationEffectAwareInterface) {
      $entity = $this;
      while ($entity = $entity->getParent()) {
        if ($entity instanceof EntityInterface) {
          $value->setEntity($entity);
          break;
        }
      }
    }
    parent::setValue($value, $notify);
  }

}
