<?php

namespace Drupal\block_generation\Plugin\views\row;

use Drupal\views\Plugin\views\row\Fields as ViewsFields;

/**
 * The basic 'fields' row plugin
 *
 * This displays fields one after another, giving options for inline
 * or not.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "block_generation_fields",
 *   title = @Translation("Block generation Fields"),
 *   help = @Translation("Displays the fields with an optional template."),
 *   theme = "block_generation_views_view_fields",
 *   display_types = {"normal"}
 * )
 */
class Fields extends ViewsFields {

}
