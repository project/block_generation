<?php

namespace Drupal\block_generation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the block effect plugin annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class BlockGenerationEffect extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The block effect label.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
