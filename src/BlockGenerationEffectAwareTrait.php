<?php

namespace Drupal\block_generation;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a default implementation of \Drupal\block_generation\BlockGenerationEffectAwareInterface.
 */
trait BlockGenerationEffectAwareTrait {

  /**
   * The collection.
   *
   * @var \Drupal\Core\Entity\EntityInterface $entity
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
    return $this;
  }

}
